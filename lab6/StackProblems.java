import java.util.ArrayList;
import java.util.Stack;

public class StackProblems {

	public static void main(String[] args) {
		Stack <Integer> s=new Stack<>();
		s.push(1);
		s.push(1);
		s.push(99);
		s.push(99);
		
		s.push(6);
		s.push(7);
		int result=sumSkipDuplicates(s);
		System.out.println(result);
		System.out.println(s);
//		System.out.println(s.peek());
//		System.out.println(s.pop());
//		System.out.println(s.pop());
//		
//		System.out.println(s);
	}

	/*
	 * Computes the sum of all the numbers in the stack before the first 99 is
	 * encountered. If no 99 is encountered, just add everything in the stack.
	 */
	public static int sumTo99(Stack<Integer> data) {
		int result=0;
		
		while(!data.isEmpty() && data.peek()!=99 ){
			result=result+data.pop();
		}
		
		return result;
	}

	/*
	 * Computes the sum of all the numbers in the stack. However, if two or more
	 * numbers IN A ROW are equal, only add one of them. So, for example, if the
	 * stack contained 4, 1, 2, 2, 7, 2, 8, 8, 8, 4, then the numbers that would be
	 * added would be 4 + 1 + 2 + 7 + 2 + 8 + 4 = 28
	 */
	public static int sumSkipDuplicates(Stack<Integer> data) {
		int result =0;
		if(data.isEmpty())
			return 0;
		int previous=data.peek();
		result=result+data.pop();
		while(!data.isEmpty()){
			if(data.peek()!=previous){
				previous=data.peek();
				result=result+data.pop();
			}
			else
				previous=data.pop();
		}
		
		return result;
	}

	/*
	 * Puts all of the characters of a string into a stack, with the first character
	 * of the string at the bottom of the stack and the last character of the string
	 * at the top of the stack.
	 */
	public static Stack<Character> stringToStack(String s) {
		Stack<Character> result=new Stack<Character>();
		for(int i=0;i<s.length();i++){
			result.push(s.charAt(i));
		}
		return result;

	}

	/*
	 * Copies a given stack, returning a new stack with the same values.
	 * Though your method might modify the original stack, the original
	 * stack should be returned to its original state before the method
	 * ends.
	 */
	public static Stack<Integer> copyStack(Stack<Integer> s) {
		Stack<Integer> result=new Stack<Integer>();
		
		ArrayList<Integer> temp=new ArrayList<Integer>();
		while(!s.isEmpty()){
			temp.add(s.pop());
		}
		for(int i=temp.size()-1;i>=0;i--){
			result.push(temp.get(i));
			s.push(temp.get(i));
		}
		return result;
	}
	
	
	/*
	 * Reverses a given stack, so that the top of the stack becomes the bottom and
	 * the bottom becomes the top.
	 */
	public static void reverseStack(Stack<Integer> s) {
		ArrayList<Integer> temp=new ArrayList<Integer>();
		while(!s.isEmpty()){
			temp.add(s.pop());
		}
		for(int i=0;i<temp.size();i++){
			s.push(temp.get(i));
		}
	}

	/*
	 * A palindrome reads the same forward and backward. Use a stack to determine if
	 * a given string is a palindrome. Challenge: try not to use any additional
	 * variables (except a counter for any loop). Just the given string and a stack
	 * of Characters.
	 */
	public static boolean isPalindrome(String s) {
		for(int i=0;i<s.length();i++){
			if(s.charAt(i)!=s.charAt(s.length()-i-1))
				return false;
		}
		return true;
	}

}
