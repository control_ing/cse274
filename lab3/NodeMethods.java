/*
 * CSE 274 Wang Bo wangb12
 */
public class NodeMethods {

	public static void main(String[] args) {
		//all testing goes here
		Node n=build123();
		//check;
		
		System.out.println(nodesToString(n));
		n=addToHead(n, 10);
		System.out.println(nodesToString(n));
		n=removeHead(n);
		System.out.println(nodesToString(n));
		n=addToTail(n, 9);
		System.out.println(nodesToString(n));
		Node m=build1To100();
		System.out.println(nodesToString(m));
		System.out.println(countNodes(m));
	}

	/*
	 *  **** Problem 1: Returns a set of linked nodes 1 -> 2 -> 3 by returning
	 * the head of that node.
	 */
	public static Node build123() {
		Node n =new Node(1);
		n.next=new Node(2);
		n.next.next=new Node(3);
		return n;
	}

	/*
	 *  **** Problem 2: Returns the contents of a list of linked nodes, beginning
	 * with the specified node
	 */
	public static String nodesToString(Node head) {
		String result="[";
		
		//grab the data
		while(head!=null){
			result=result+head.data+" ";
			head=head.next;
		}
		
		result=result+"]";
		return result;
	}

	/*
	 *  **** Problem 3: Adds the specified value to the start of a list of linked
	 * nodes. NOTE: The head of the list will change, which is why it is
	 * necessary to return a new node that marks the head of the linked nodes.
	 */
	public static Node addToHead(Node head, int value) {
		Node newNode=new Node(value);
		newNode.next=head;
		return newNode;
	}

	/*
	 *  **** Problem 4: Removes the first item from the list. If the list
	 * contains only one node, returns null. If the head is null to begin with,
	 * also returns null
	 */
	public static Node removeHead(Node head) {
		return head.next;
	}

	/*
	 *  **** Problem 4: Returns a set of linked nodes 1 -> 2 -> 3 .... all the
	 * way to n returning the head of that node.
	 */
	public static Node build1To100() {
		Node n=new Node(1);
		Node x=n;
		for(int i=2;i<101;i++){
			x.next=new Node(i);
			x=x.next;
		}
		return n;
	}

	/*
	 * **** Problem 6: Returns a count of the number of nodes in a list
	 */
	public static int countNodes(Node head) {
		int count=1;
		Node n=head;
		while(n.next!=null){
			count++;
			n=n.next;
		}
		return count;
	}
	
	/*
	 * **** Problem 7: Returns true if the list contains the value, and false
	 * otherwise.
	 */
	public static boolean contains(Node head, int value) {
		Node n=head;
		while(n!=null){
			if(n.data==value)
				return true;
			n=n.next;
		}
		
		return false;
	}
	
	/*
	 * **** Problem 8: Returns the last value in a list
	 */
	public static int lastValue(Node head) {
		Node n=head;
		while(n.next!=null){
			n=n.next;
		}
		return n.data;
	}
	
	/*
	 *  **** Problem 9: Adds the specified value to the end of a list of linked
	 * nodes. NOTE: The head of the list usually won't change, but could change if
	 * there is only one node in the list.
	 */
	public static Node addToTail(Node head, int value) {
		Node n=head;
		while(n.next!=null){
			n=n.next;
		}
		n.next=new Node(value);
		return head;
	}

	/*
	 *  **** Problem 10: Removes the last item from the list. If the list
	 * contains only one node, returns null. 
	 */
	public static Node removeTail(Node head) {
		if(head.next==null)
			return null;
		Node lastSecond =head;
		while(lastSecond.next.next!=null){
			lastSecond=lastSecond.next;
		}
		lastSecond.next=null;
		
		return head;
	}
	
	
	/*
	 * **** Problem 11: Returns the head a linked list of n random nodes.
	 * Assume length >= 0
	 */
	public static Node buildRandomNodes(int length) {
		Node n=new Node((int)(length*Math.random()));
		Node m=n;
		
		for(int i=0;i<length-1;i++){
			Node q=new Node((int)(length*Math.random()));
			m.next=q;
			m=m.next;
			
		}
		return n;
	}
	
	
}
