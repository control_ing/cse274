
public class BinarySearchTree {

	private Node root;

	public BinarySearchTree() {
		root = null;
	}

	/*
	 * Adds the specified node to the BST
	 */
	public String add(String s) {

		// root is a special case:
		if (root == null) {
			root = new Node(s);
			return s;
		}

		Node current = root;

		while (current != null) {
			if (current.data.equals(s)) // no duplicates
				return s;
			else if (current.data.compareTo(s) > 0) { // go left
				if (current.left == null) { // found nothing? Add node here
					current.left = new Node(s);
				} else {
					current = current.left;
				}
			} else { // go right
				if (current.right == null) { // found nothing? Add node here
					current.right = new Node(s);
				} else {
					current = current.right;
				}
			}
		}
		return s;
	}

	/*
	 * Returns true if the string is found in the BST
	 */
	public boolean contains(String s) {

		Node current = root;

		while (current != null) {
			if (current.data.equals(s))
				return true;
			if (current.data.compareTo(s) > 0) {
				current = current.left;
			} else {
				current = current.right;
			}
		}

		// Made it here? Not found!
		return false;
	}

	/*
	 * Removes the specified string from the BST
	 */
	public boolean remove(String s) {
		if (root == null)
			return false;
		if (!contains(s))
			return false;
		if (root.data.equals(s)) {
			if (root.left == null || root.right == null) {
				root = root.left == null ? root.right : root.left;
			} else {
				Node maxleft = max(root.left);
				remove(maxleft.data);
				root.data = maxleft.data;
			}
		} else {
			boolean left = false;
			Node parent = null;
			Node current = root;
			while (current != null && current.data.compareTo(s) != 0) {
				if (current.data.compareTo(s) > 0) {
					left = true;
					parent = current;
					current = current.left;
				} else if (current.data.compareTo(s) < 0) {
					left = false;
					parent = current;
					current = current.right;
				}
			}
			if (numOfChild(current) == 0) {
				if (left)
					parent.left = null;
				else
					parent.right = null;
			} else if (numOfChild(current) == 1) {
				if (left)
					parent.left = current.left == null ? current.right : current.left;
				else
					parent.right = current.left == null ? current.right : current.left;
			} else {
				Node maxleft = max(current.left);
				remove(maxleft.data);
				current.data = maxleft.data;
			}
		}
		return true;
	}

	public String max() {
		if (root == null)
			return null;
		return max(root).data;
	}

	private Node max(Node n) {
		if (n == null)
			return null;
		while (n.right != null) {
			n = n.right;
		}
		return n;
	}

	private int numOfChild(Node n) {
		if (n.left == null && n.right == null)
			return 0;
		else if (n.left == null || n.right == null)
			return 1;
		else
			return 2;
	}

	/*
	 * Prints the values in order
	 */
	public void inorderTraversal() {
		inorderTraversal(root);
		System.out.println();
	}

	/*
	 * Recursive helper for traversing
	 */
	private void inorderTraversal(Node n) {
		if (n != null) {
			inorderTraversal(n.left);
			System.out.print(n.data + " ");
			inorderTraversal(n.right);
		}
	}

	/*
	 * Basic Binary Tree Node
	 */
	private class Node {
		private String data;
		private Node left, right;

		private Node(String data) {
			this.data = data;
			left = right = null;
		}
	}
}
