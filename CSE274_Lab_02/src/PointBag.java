/**
 * implementation of ADT bag
 * Finite size
 */
import java.awt.Point;
import java.util.Arrays;
public class PointBag implements BagInterface<Point>{
	
	private Point[] points;
	private int size;
	public static final int DEFAULT_CAPACITY=100;
	/**
	 * construct a bag with the specified capacity 
	 * @param capacity	the maximum number of items 
	 */
	public PointBag(int capacity){
		points=new Point[capacity];
		size=0;
	}
	/**
	 * construct a bag with a default capacity 
	 */
	public PointBag(){
		this(DEFAULT_CAPACITY);
	}
	
	@Override
	public int getCurrentSize() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size==0;
	}

	/**
	 * adds the specified point if the is room
	 * returns true if the point is successfully added,
	 * false if not 
	 */
	public boolean add(Point newEntry) {
		if(size<points.length){
			points[size]=newEntry;
			size++;
			return true;
		}
		return false;
	}

	@Override
	public Point remove() {
		if(size==0)
		return null;
		Point result=points[size-1];
		points[size-1]=null;
		size--;
		return result;
	}

	@Override
	public boolean remove(Point anEntry) {
		 for(int i=0;i<size;i++){
			 if(points[i].equals(anEntry)){
				 points[i]=points[size-1];
				 points[size-1]=null;
				 size--;
				 return true;
			 }
		 }
		 return false;
	}

	@Override
	public void clear() {
		for(int i=0;i<size;i++){
			points[i]=null;
		}
		size=0;
	}

	@Override
	public int getFrequencyOf(Point anEntry) {
		int frequency=0;
		for(int i=0;i<size;i++){
			if(points[i].equals(anEntry))
				frequency++;
		}
		return frequency;
	}

	@Override
	public boolean contains(Point anEntry) {
		for(int i=0;i<size;i++){
			if(points[i].equals(anEntry))
				return true;
		}
		return false;
	}

	@Override
	public Point[] toArray() {
		Point[] out=new Point[size];
		for(int i=0;i<size;i++){
			out[i]=points[i];
		}
		return out;
	}
	
	public String toString(){
		return Arrays.toString(points);
	}
}
