import java.util.Arrays;

/**
 * @author Bo Wang wangb12
 * CSE 274 Project 2
 */
public class SetTester {

	public static void main(String[] args) {
		System.out.println("Create a list without spicific size,put 1,2,3");
		ResizableArraySet list1=new ResizableArraySet();
		System.out.print("put 1,true? ");
		System.out.println(list1.add(1));
		System.out.print("put 1 again,false? ");
		System.out.println(list1.add(1));
		System.out.print("put 2,true? ");
		System.out.println(list1.add(2));
		System.out.print("put 3,true? ");
		System.out.println(list1.add(3));
		System.out.println(Arrays.toString(list1.toArray()));
		System.out.println("========================================");
		System.out.println("Create a list spicific size 2,put 4,5,6");
		ResizableArraySet list2=new ResizableArraySet();
		System.out.print("put 4,true? ");
		System.out.println(list2.add(4));
		System.out.print("put 4 again,false? ");
		System.out.println(list2.add(4));
		System.out.print("put 5,true? ");
		System.out.println(list2.add(5));
		System.out.print("put 6,true? ");
		System.out.println(list2.add(6));
		System.out.print("put 7,true? ");
		System.out.println(list2.add(7));
		System.out.println("========================================");
		System.out.print("how many numbers in here? 4? ");
		System.out.println(list2.getSize());
		System.out.println("========================================");
		System.out.println(Arrays.toString(list2.toArray()));
		System.out.print("does this set contain 4? true? ");
		System.out.println(list2.contains(4));
		System.out.print("does this set contain 1? false? ");
		System.out.println(list2.contains(1));
		System.out.println("========================================");
		System.out.print("is this list empty? false? ");
		System.out.println(list2.isEmpty());
		System.out.println("========================================");
		System.out.print("can I remove num 4? true? ");
		System.out.println(list2.remove(4));
		System.out.print("list after remove, [5,6,7]? ");
		System.out.println(Arrays.toString(list2.toArray()));
		System.out.print("can I remove num 6? true? ");
		System.out.println(list2.remove(6));
		System.out.print("waht num did I remove if I want remove a unspecific num? ");
		System.out.println(list2.remove());
		System.out.print("list after remove [5]? "); 
		System.out.println(Arrays.toString(list2.toArray()));
		System.out.println("========================================");
		System.out.println("add 1,2,3,4 to the list");
		list2.add(1);
		list2.add(2);
		list2.add(3);
		list2.add(4);
		System.out.println(Arrays.toString(list2.toArray()));
		System.out.print("now clean the list. is it a empty list now? ");
		list2.clear();
		System.out.println(Arrays.toString(list2.toArray()));
		System.out.println("========================================");
		System.out.println("create two list [1,2,3,4,5] and [1,3,5,7,9,10]");
		list1.add(4);
		list1.add(5);
		list2.add(1);
		list2.add(3);
		list2.add(5);
		list2.add(7);
		list2.add(9);
		list2.add(10);
		System.out.println(Arrays.toString(list1.toArray())+Arrays.toString(list2.toArray()));
		System.out.println("what's the union of those two? ");
		System.out.println(Arrays.toString(list1.union(list2).toArray()));
		System.out.println("what's the intersection of those two? ");
		System.out.println(Arrays.toString(list1.intersection(list2).toArray()));
		System.out.println("what are numbers that just the first have? ");
		System.out.println(Arrays.toString(list1.difference(list2).toArray()));
		
		
	}

}
