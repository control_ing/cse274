/**
 * @author Bo Wang wangb12 
 * CSE 274 Project 2
 *
 */
public class ResizableArraySet implements SetInterface {
	private int entries=0;
	private int[] list;
	
	public ResizableArraySet() {
		list=new int[10];
	}
	
	public ResizableArraySet(int size){
		list=new int[size];
	}
	
	/* (non-Javadoc)
	 * @see SetInterface#getSize()
	 */
	@Override
	public int getSize() {
		return entries;
	}

	/* (non-Javadoc)
	 * @see SetInterface#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return entries==0;
	}

	/* (non-Javadoc)
	 * @see SetInterface#add(int)
	 */
	@Override
	public boolean add(int newValue) {
		for(int i=0;i<entries;i++){
			if(list[i]==newValue)
				return false;
		}
		
		if(entries>=list.length){
			int[] a=new int[list.length*2];
			for(int i=0;i<list.length-15;i++){
				a[i]=list[i];
			}
			list=a;
		}
		list[entries]=newValue;
		entries++;
		return true;
	}

	/* (non-Javadoc)
	 * @see SetInterface#remove(int)
	 */
	@Override
	public boolean remove(int aValue) {
		for(int i=0;i<entries;i++){
			if(list[i]==aValue){
				for(int a=i;a<entries;a++){
					list[a]=list[a+1];
				}
				entries--;
				return true;
			}
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see SetInterface#remove()
	 */
	@Override
	public int remove() {
		if (entries==0){
			return 0;
		}
		int out =list[entries-1];
		list[entries-1]=0;
		entries--;
		return out;
	}

	/* (non-Javadoc)
	 * @see SetInterface#clear()
	 */
	@Override
	public void clear() {
		list=new int[list.length];
		entries=0;
	}

	/* (non-Javadoc)
	 * @see SetInterface#contains(int)
	 */
	@Override
	public boolean contains(int anEntry) {
		for(int i:list){
			if(i==anEntry)
				return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see SetInterface#union(SetInterface)
	 */
	@Override
	public SetInterface union(SetInterface anotherSet) {
		SetInterface out=new ResizableArraySet();
		for(int a:list){
			if(!out.contains(a))
				out.add(a);
		}
		int[] another=anotherSet.toArray();
		for(int b:another){
			if(!out.contains(b))
				out.add(b);
		}
		return out;
	}

	/* (non-Javadoc)
	 * @see SetInterface#intersection(SetInterface)
	 */
	@Override
	public SetInterface intersection(SetInterface anotherSet) {
		SetInterface out=new ResizableArraySet();
		for(int i=0;i<entries;i++){
			if(anotherSet.contains(list[i]))
				out.add(list[i]);
		}
		return out;
	}

	/* (non-Javadoc)
	 * @see SetInterface#difference(SetInterface)
	 */
	@Override
	public SetInterface difference(SetInterface anotherSet) {
		SetInterface out=new ResizableArraySet();
		for(int a:list){
			if(!anotherSet.contains(a))
				out.add(a);
		}
		return out;
	}

	/* (non-Javadoc)
	 * @see SetInterface#toArray()
	 */
	@Override
	public int[] toArray() {
		int[] out=new int[entries];
		for(int i=0;i<entries;i++){
			out[i]=list[i];
		}
		return out;
	}

}
