/**
 * A binary tree implementation. When adding values to
 * a tree, there is no assumed "correct" location for that value.
 * So, we will give the root package access, so that we can manually
 * build our trees from a tester class.
 *
 * @author Norm Krumpe
 *
 */

public class BinaryTree {
	Node root;
	
	/**
	 * Returns the height of this tree
	 * @return
	 */
	public int getHeight() {
		return getHeight(root); 
	}
	
	private int getHeight(Node n){
		if(n==null)
			return 0;
		else{
			return 1+Math.max(getHeight(n.left),getHeight(n.right));
		}
	}

	public int getNumberOfNodes() {
		return getNumberOfNodes(root); 
	}
	
	private int getNumberOfNodes(Node n){
		if(n==null){
			return 0;
		}
		else{
			return 1+getNumberOfNodes(n.left)+ getNumberOfNodes(n.right);
		}
	}
	
	public int getNumberOfLeaves() {
		return getNumberOfLeaves(root); 
	}
	
	private int getNumberOfLeaves(Node n){
		if(n==null)
			return 0;
		else if(n.left==null&&n.right==null)
			return 1;
		return getNumberOfLeaves(n.left)+getNumberOfLeaves(n.right);
	}
	
	/**
	 * Prints the preorder traversal of this tree
	 */
	public void preorderTraversal() {
		preorderTraversal(this.root);
		System.out.println();
	}
	
	private void preorderTraversal(Node n){
		if(n!=null){
		System.out.println(n.data+" ");
		preorderTraversal(n.left);
		preorderTraversal(n.right);
		}
	}
	
	/**
	 * Prints the inorder traversal of this tree
	 */
	public void inorderTraversal() {
		inorderTraversal(root);
	}

	private void inorderTraversal(Node n){
		if(n!=null){
			inorderTraversal(n.left);
			System.out.println(n.data);
			inorderTraversal(n.right);
		}
	}
	
	/**
	 * Prints the postorder traversal of this tree
	 */
	public void postorderTraversal() {
		postorderTraversal(root);
	}

	private void postorderTraversal(Node n){
		if(n!=null){
			postorderTraversal(n.left);
			postorderTraversal(n.right);
			System.out.println(n.data);
		}
	}
	
	// Returns true if the tree is full,
	// and false otherwise
	public boolean isFull() {
		return isFull(root);
	}

	private boolean isFull(Node n){
		if(n!=null){
			if((n.left!=null&&n.right==null)||(n.left==null&&n.right!=null))
				return false;
			else if(n.left!=null&&n.right!=null&&getHeight(n.left)!=getHeight(n.right))
				return false;
			return isFull(n.left)&&isFull(n.right);
		}
		return true;
	}
	
	// Returns true if the tree contains the value,
	// and false otherwise
	public boolean contains(int value) {
		return contains(root,value);
	}
	
	private boolean contains(Node n,int value){
		while(n!=null){
			if(n.data==value)
				return true;
			return contains(n.left,value)||contains(n.right,value);
		}
		return false;
	}
	
	// Returns the largest value in the tree
	public int getMax() {
		return getMax(root);
	}
	
	private int getMax(Node n){
		int Max=Integer.MIN_VALUE;
		if(n!=null){
			Max=n.data;
		return Math.max(Math.max(getMax(n.left), getMax(n.right)),n.data);
		}
		return Max;
	}
}
