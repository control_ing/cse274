
public class MyTester {
	public static void main(String[] args){
		BinaryTree bt =new BinaryTree();
		bt.root =new Node(7);
		
		//left subtree
		bt.root.left=new Node(1);
		bt.root.left.left=new Node(9);
		bt.root.left.left.right=new Node(2);
		
		bt.root.right=new Node(6);
		bt.root.right.left=new Node(8);
		bt.root.right.right=new Node(5);
		bt.root.right.right.left=new Node(4);
		
		System.out.println(bt.getHeight());
		System.out.println(bt.getNumberOfLeaves());
		
		BinaryTree bt2 =new BinaryTree();
		bt2.root=new Node(1);
		bt2.root.left=new Node(2);
		bt2.root.right=new Node(3);
		System.out.println(bt2.isFull());
		
		bt.inorderTraversal();
		System.out.println();
		bt.postorderTraversal();
	}
}
