import java.util.Arrays;

/**
 * cse 274
 * @author wangb12 Bo Wang
 *
 */
public class SetTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SortedSet set=new SortedSet();
		System.out.print("add a,b,c,d,e to the set: \nExpecte: [a b c d e ]\nResult:  ");
		set.add("a");
		set.add("b");
		set.add("c");
		set.add("d");
		set.add("e");
		System.out.println(set+"\n");
		
		SortedSet set2=new SortedSet();		
		System.out.print("add e,d,c,b,a to the set: \nExpecte: [a b c d e ]\nResult:  ");
		set2.add("e");
		set2.add("d");
		set2.add("c");
		set2.add("b");
		set2.add("a");
		System.out.println(set2+"\n");
		
		SortedSet set3=new SortedSet();
		System.out.print("add e,d,b,a,c to the set: \nExpecte: [a b c d e ]\nResult:  ");
		set3.add("e");
		set3.add("d");
		set3.add("b");
		set3.add("a");
		set3.add("c");
		System.out.println(set+"\n");
		
		System.out.print("Remove e from [a b c d e ]: \nExpecte: [a b c d ]\nResult:  ");
		set.remove("e");
		System.out.println(set+"\n");
		
		System.out.print("Remove a from [a b c d e ]: \nExpecte: [b c d e ]\nResult:  ");
		set2.remove("a");
		System.out.println(set2+"\n");
		
		System.out.print("Remove c from [a b c d e ]: \nExpecte: [a b d e ]\nResult:  ");
		set3.remove("c");
		System.out.println(set3+"\n");
		
		SortedSet empty=new SortedSet();
		System.out.print("Remove a form empty set:\nExpecte: []\nResult:  ");
		empty.remove("a");
		System.out.println(empty+"\n");
		
		System.out.print("Remove unspecified string from [a b c d e ]:\nExpecte: [b c d ]\nResult:  ");
		set.remove();
		System.out.println(set+"\n");
		
		System.out.print("Remove unspecified string from empty set:\nExpecte: []\nResult:  ");
		empty.remove();
		System.out.println(empty+"\n");
		
		System.out.print("What is current size?\nExpecte: 3\nResult:  ");
		System.out.println(set.getCurrentSize()+"\n");
		
		System.out.print("Is empty set empty?\nExpecte: true\nResult:  ");
		System.out.println(empty.isEmpty()+"\n");
		
		System.out.print("Is [b c d ] a empty set?\nExpecte: false\nResult:  ");
		System.out.println(set.isEmpty()+"\n");
		
		System.out.print("Clear a set of [a b d e ]\nExpecte: []\nResult:  ");
		set3.clear();
		System.out.println(set3);
		
		System.out.print("Does [b c d e ] contains a?b?d?e?z?\n"
				+ "Expecte: false,true,true,true,fasle\nResult:  ");
		System.out.println(set2.contains("a")+","+set2.contains("b")+
				","+set2.contains("d")+","+set2.contains("e")+","+set2.contains("z"));
		System.out.print("[b c d ] to Array: \nExpected: [b,c,d]\nResult:  ");
		System.out.println(Arrays.toString(set.toArray()));
	}

}
