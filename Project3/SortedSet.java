/**
 * A linked-node implementation of the Set ADT in which elements of the set
 * are always sorted (in this case, lexicographically, which is a fancy
 * way of saying "alphabetically").  Note that the String class has a compareTo
 * method which you should be using to assist you in keeping the set sorted.
 * 
 * 
 */
public class SortedSet implements SetInterface<String> {
	private Node head;
	private int size;
	public SortedSet(){
		head=null;
		size=0;
	}
	@Override
	public int getCurrentSize() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size==0;
	}
	@Override
	public boolean add(String newEntry) {
		if(this.contains(newEntry))//check duplicates 
			return false;
		Node node=new Node(newEntry);
		if(head==null){//when there is nothing in set
			head=node;
			size++;
			return true;
		}
		Node n=head;
		if(n.data.compareTo(newEntry)>0){//compare the first string with new entry
			node.next=head;
			head=node;
			size++;
			return true;
		}
		while(n.next!=null){//compare remain strings with new entry 
			if(n.next.data.compareTo(newEntry)>0){
				node.next=n.next;
				n.next=node;
				size++;
				return true;
			}
			n=n.next;
		}
		n.next=node;//everything is less than new entry here, so put it at the end
		size++;
		return true;
	}
	
	@Override
	public boolean remove(String anEntry) {
		//check is the first string is the one that need to be removed
		if(head==null)
			return false;
		if(head.data.equals(anEntry)){
			head=head.next;
			size--;
			return true;
		}
		Node n=head;
		while(n.next!=null){
			if(n.next.data.equals(anEntry)){
				n.next=n.next.next;
				size--;
				return true;
			}
			n=n.next;
		}
		return false;
	}
	
	@Override
	public String remove() {
		if(head==null)
			return null;
		String data=head.data;
		head=head.next;
		size--;
		return data;
	}

	@Override
	public void clear() {
		head=null;
		size=0;
	}

	@Override
	public boolean contains(String anEntry) {
		Node n=head;
		  while(n!=null){
		   if(n.data.equals(anEntry))
		    return true;
		   n=n.next;
		  }
		  
		  return false;
	}

	/*
	 * returns a string representation of the items in the bag,
	 * specifically a space separated list of the strings, enclosed
	 * in square brackets [].  For example, if the set contained
	 * cat, dog, then this should return "[cat dog]".  If the
	 * set were empty, then this should return "[]".
	 */
	
	@Override
	public String toString() {
		String result = "[";
		  Node current = head;
		  while (current != null) {
		   result = result + current.data + " ";
		   current = current.next;
		  }
		  result=result+"]";
		  return result;
	}
	
	@Override
	public String[] toArray() {
		String[] out=new String[size];
		  Node n=head;
		  for(int i=0;i<size;i++){
		   out[i]=n.data;
		   n=n.next;
		  }
		  return out;
	}

	private class Node{
		private String data;
		private Node next;
		
		private Node(String data){
			this.data=data;
		}
	}
	
}
