import java.io.File;
import java.util.Scanner;

public class Driver {

	public static void main(String[] args) throws Exception {

		Name name1 = new Name("Jean", "A", "Smith");
		Name name2 = new Name("Jean", "A", "Smith");
		Name name3 = new Name("Amy", "B", "Jones");
		Name name4 = new Name("Amy", "C", "Jones");

		///////////////////////////////////////////////////////////////
		// #1:      Implement equals and implement the Comparable interface
		//          for the Name class.
		//          For comparing names, names should be sorted by last name first.
		//          If two people have the same last name, then compare them by their
		//          first name and middle initial.  For example, "Dusty W. Morning" 
		//          is before "Amy B. Night" (because Morning is before Night).
		//			"Dawn A. Johnson" is before "Dawn Z. Johnson" and both of them
		//          are before "Sarah A. Johnson".
		
		///////////////////////////////////////////////////////////////
		// #2: 		Verify that all these print statements give the correct results.
		System.out.println("These two calls to equals should be true: ");
		System.out.println(name1.equals(name2));
		System.out.println(name1.equals(name1)); 
		
		System.out.println("These two calls to equals should be false: ");
		System.out.println(name1.equals("cat"));
		System.out.println(name1.equals(null)); 

		// This should return a negative value because Jones is before Smith.
		System.out.println("should be negative: " + name3.compareTo(name1));

		// This should return a negative value because both have the name Jones
		// but Amy B is before Amy C.
		System.out.println("should be negative: " + name3.compareTo(name4));
		
		// This should return a positive value because Smith is after Jones.
		System.out.println("should be positive: " + name1.compareTo(name3));
		
		// These should return zero because the two names are equal:
		System.out.println("should be zero: " + name1.compareTo(name2));
		System.out.println("should be zero: " + name2.compareTo(name1));
		
		///////////////////////////////////////////////////////////////
		// Here is a SortedDictionary
		// Like all dictionaries, you add two pieces: a KEY and a VALUE
		// The KEY is what you use to find the VALUE.
		// For example, if you want someone's GPA, you look them up
		// by name (that's the key) so that you can get their GPA (that's the value).
		DictionaryInterface<Name, Double> grades = new SortedArrayDictionary<>();
		
		///////////////////////////////////////////////////////////////
		// #4: 		For each of the following, write one line of code to do what is asked.
		
		// Amy A Young's GPA is 3.9.  Add Amy to the dictionary.  Print what is returned.
		Name amy=new Name("Amy","A","Young");
		System.out.println(grades.add(amy, 3.9));
		
		// Amy A Young's GPA changed to 3.0.  Update her information. Print what is returned.
		System.out.println(grades.add(amy, 3.0));

		
		// Display Amy's GPA.
		System.out.println(grades.getValue(amy));

		
		// Steve X Buscemi's GPA is 2.5.  Add him to the dictionary.
		Name steve=new Name("Steve","X","Buscemi");
		grades.add(steve, 2.5);
		
		// Amy B Young (she's the sister of Amy A Young) has a gpa of 2.99.  Add her.
		Name amyB=new Name("Amy","B","Young");
		grades.add(amyB, 2.99);

		
		// Print the contents of the dictionary. Verify that it is sorting correctly.
		// Just uncomment this line:
		 System.out.println(grades);
		
		// Remove Steve.
		grades.remove(steve);
		
		// Remove everyone from the dictionary
		grades.clear();
		///////////////////////////////////////////////////////////////
		// TODO #5: 		gradeData.txt contains 100 lines of GPA data.  
		// Add all of them to the dictionary.
		File file=new File("gradeData.txt");
		Scanner in=new Scanner(file);
		for(int i=0;i<100;i++){
			Name name=new Name(in.next(),in.next(),in.next());
			grades.add(name, in.nextDouble());
		}
		//#6 print dictronary, size, gpa for "Nita E Cotta"
		System.out.println(grades.getValue(new Name("Nita","E","Cotta")));
	}

}
