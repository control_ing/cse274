import java.util.Arrays;
import java.util.EmptyStackException;

/**
 * 
 * @author Bo Wang wangb12
 *
 */
public class ArrayStack<T> implements StackInterface<T> {
	private int size;
	private T[] stack;

	@SuppressWarnings("unchecked")
	ArrayStack() {
		size = 0;
		stack = (T[]) new Object[10];
	}
	
	
	@Override
	public void push(T newEntry) {
		if(size==stack.length){
			stack=Arrays.copyOf(stack, stack.length*2);
		}
		stack[size] = newEntry;
		size++;
	}

	@Override
	public T pop() {
		if(size==0){
			throw new EmptyStackException();
		}
		T temp = stack[size - 1];
		stack[size - 1] = null;
		size--;
		return temp;
	}

	@Override
	public T peek() {
		if(size==0){
			throw new EmptyStackException();
		}
		return stack[size - 1];
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void clear() {
		size = 0;
		stack = (T[]) new Object[10];
	}
	
	
}
