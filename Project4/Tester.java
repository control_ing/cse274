import java.util.Scanner;

/**
 * 
 */

/**
 * @author Bo Wang wangb12
 *
 */
public class Tester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.print("Infix expression: ");
		Scanner scanner=new Scanner(System.in);
		String input=scanner.nextLine();
		scanner.close();
		try{
		InfixExpression exp=new InfixExpression(input);
		System.out.println(exp.getPostfixExpression());
		System.out.println(exp.evaluate());
		}
		catch (IllegalArgumentException e){
			System.out.println("Fail: Invalid expression");
		}
		
	}

}
