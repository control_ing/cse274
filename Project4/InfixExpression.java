import java.util.ArrayList;
import java.util.Scanner;

/**
 * 
 * @author Bo Wang wangb12
 *
 */
public class InfixExpression {
	private String infix;

	/**
	 * construct a new InfixExpression
	 * 
	 * @param input
	 *            the infix that user input
	 */
	public InfixExpression(String input) {
		infix = input;
		clean();
		if (!isValid()) {
			throw new IllegalArgumentException();
		}

	}

	/**
	 * simply give back the infix
	 */
	public String toString() {
		return infix;
	}

	/**
	 * This private method is used for check if the infix expression has
	 * balanced parentheses(same amount of "(" and ")" )
	 * 
	 * @return return true if the infix expression has balanced parentheses, and
	 *         false otherwise
	 */
	private boolean isBalanced() {
		int countOpen = 0;
		int countClose = 0;
		for (int i = 0; i < infix.length(); i++) {
			if (infix.charAt(i) == '(')
				countOpen++;
			else if (infix.charAt(i) == ')')
				countClose++;
		}
		return countClose == countOpen;
	}

	/**
	 * This private method should return true if the infix expression is valid
	 * in all respects, and false otherwise
	 * 
	 */
	private boolean isValid() {
		if (!isBalanced())
			return false;
		if(numCount()==0)
			return false;
		Scanner scanner = new Scanner(infix);
		ArrayList<String> list = new ArrayList<>();
		while (scanner.hasNext()) {
			String current = scanner.next();
			list.add(current);
		}
		scanner.close();
		if ((level(list.get(0)) != 5 && level(list.get(0)) != 1)
				|| (level(list.get(list.size() - 1)) != 5 && level(list.get(list.size() - 1)) != 1))
			return false;
		else {
			for (int i = 1; i < list.size() - 1; i++) {
				if (level(list.get(i)) == 1 && level(list.get(i - 1)) == 5 && level(list.get(i + 1)) == 5)
					return false;
				if (level(list.get(i)) < 5 && level(list.get(i)) > 1) {
					if ((level(list.get(i - 1)) != 5 && level(list.get(i - 1)) != 1)
							|| (level(list.get(i + 1)) != 5 && level(list.get(i + 1)) != 1))
						return false;
				}
			}
		}
		return true;
	}
	/**
	 * count how many operator in the expression
	 * @return total number of operator
	 */
	private int operatorCount(){
		int operator = 0;
		for (int i = 0; i < infix.length(); i++) {
			if (level(infix.charAt(i) + "") != 5)
				operator++;
		}
		return operator;
	}
	/**
	 * count how many numbers in the expression
	 */
	private int numCount(){
		Scanner scanner=new Scanner(infix);
		int operator=0;
		while(scanner.hasNext()) {
			if (level(scanner.next()) == 5)
				operator++;
		}
		scanner.close();
		return operator;
	}
	/**
	 * Clean up the format of the input
	 */
	private void clean() {
		
		if (operatorCount() == 0)
			infix = infix.trim();
		else {
			Scanner scanner = new Scanner(infix);
			String temp = "";
			while (scanner.hasNext()) {
				temp = temp + scanner.next();
			}
			scanner.close();
			infix = "";
			for (int i = 0; i < temp.length(); i++) {
				if (level(temp.charAt(i) + "") == 5)
					infix += temp.charAt(i);
				else {
					infix += " " + temp.charAt(i) + " ";
					
				}
			}
		}

	}

	/**
	 * This method should return the postfix expression that corresponds to the
	 * given infix expression
	 * 
	 * @return postfix expression
	 */
	public String getPostfixExpression() {
		String result = "";
		ArrayStack<String> stack = new ArrayStack<>();
		Scanner scanner = new Scanner(infix);
		while (scanner.hasNext()) {
			String current = scanner.next();
			if (level(current) == 1) {
				if (stack.isEmpty())
					stack.push(current);
				else if (current.equals("(")) {
					stack.push(current);
				} else {
					while (!stack.peek().equals("(")) {
						result += stack.pop() + " ";
					}
					stack.pop();
				}
			} else if (level(current) == 2) {
				if (stack.isEmpty())
					stack.push(current);
				else {
					while (!stack.isEmpty() && level(stack.peek()) < 2) {
						result += stack.pop() + " ";
					}
					stack.push(current);
				}
			} else if (level(current) == 3 || level(current) == 4) {
				if (stack.isEmpty())
					stack.push(current);
				else {
					while (!stack.isEmpty() && level(stack.peek()) <= level(current) && level(stack.peek()) > 1) {
						result += stack.pop() + " ";
					}
					stack.push(current);
				}
			} else {
				result = result + current + " ";
			}
		}
		scanner.close();
		while (!stack.isEmpty()) {
			result += stack.pop() + " ";
		}
		result = result.trim();
		return result;

	}

	/**
	 * evaluate the infix expression
	 * 
	 * @return the result of the expression
	 */
	public int evaluate() {
		if(operatorCount()==0)
			return -1;
		String post = getPostfixExpression();
		ArrayStack<Integer> numStack = new ArrayStack<>();
		Scanner scanner = new Scanner(post);
		while (scanner.hasNext()) {
			String current = scanner.next();
			if (level(current) == 5) {
				numStack.push(Integer.parseInt(current));
			} else {
				int result = 0;
				int top = numStack.pop();
				int sTop = numStack.pop();
				if (current.equals("+"))
					result = (int) top + sTop;
				else if (current.equals("-"))
					result = (int) sTop - top;
				else if (current.equals("*"))
					result = (int) sTop * top;
				else if (current.equals("/"))
					result = (int) sTop / top;
				else if (current.equals("%"))
					result = (int) sTop % top;
				else
					result = (int) Math.pow(sTop, top);
				numStack.push(result);
			}
		}
		scanner.close();
		return numStack.pop();
	}

	/**
	 * determine the level of the input string: ( ) would be level 1 ^ would be
	 * level 2 * / % would be level 3 + - would be level 4 others would be level
	 * 5
	 * 
	 * @param input
	 * @return
	 */
	private int level(String input) {
		if (input.equals("(") || input.equals(")"))
			return 1;
		else if (input.equals("^"))
			return 2;
		else if (input.equals("*") || input.equals("/") || input.equals("%"))
			return 3;
		else if (input.equals("+") || input.equals("-"))
			return 4;
		else
			return 5;
	}
}
