/**
 * Your implementation of the LookupInterface. The only public methods in this
 * class should be the ones that implement the interface. You should write as
 * many other private methods as needed. Of course, you should also have a
 * public constructor.
 * 
 * @author // TODO: Add your name here
 */

public class StudentLookup implements LookupInterface {
	HashMap list;
	Orray oray;

	public StudentLookup() {
		list = new HashMap();
		oray = new Orray();
	}

	@Override
	public void addString(int amount, String s) {
		list.add(s, amount);
	}

	@Override
	public int lookupCount(String s) {
		return list.getValue(s);
	}

	@Override
	public String lookupPopularity(int n) {
		return oray.get(n);
	}

	@Override
	public int numEntries() {
		return list.getCount();
	}

	// ------------------------------------------------------------------------Hash
	// map class
	private class HashMap {
		Words[] list;
		int count = 0;

		HashMap() {
			list = new Words[5000000];
		}
		
		public void resize(){
			Words[] temp=new Words[list.length*2];
			for(int i=0;i<count;i++){
				temp[i]=list[i];
			}
			list=temp;
		}
		
		public boolean add(String key, int num) {
			int hash = Math.abs(key.hashCode()) % 5000000;
			// System.out.println(key);
			while (true) {
				if (list[hash] == null) {
					// System.out.println("?");
					list[hash] = new Words(key, num);
					oray.add(new Words(key, num));
					count++;
					if(list.length-count==list.length/2)
						resize();
					return true;
				} else if (!list[hash].key.equals(key)) {
					hash++;
					// System.out.println("123");
				} else {
					// System.out.println(list[hash].getValue());
					list[hash].add(num);
					oray.increase(key, num);
					return true;
				}
			}
		}

		public int getCount() {
			return count;
		}

		public int getValue(String key) {
			int hash = Math.abs(key.hashCode()) % 5000000;
			if (list[hash] == null)
				return 0;
			while (true) {
				if (list[hash].key.equals(key))
					return list[hash].getValue();
				hash++;
			}
		}

	}

	// -----------------------------words class that store string and number of
	// times it appear
	private class Words implements Comparable {
		String key;
		int value;

		Words(String key, int value) {
			this.key = key;
			this.value = value;
		}

		int getValue() {
			return value;
		}

		void add(int num) {
			value += num;
		}

		@Override
		public int compareTo(Object o) {
			if (this.value > ((Words) o).getValue())
				return 2;
			else if (this.value < ((Words) o).getValue())
				return -2;
			else if (this.key.compareTo(((Words) o).key) < 0)
				return 1;
			else
				return -1;

		}
	}

	// ---------------------------------------------------------------------Linked
	// node class
	public class Orray {
		Words[] oray;
		int count = 0;

		Orray() {
			oray = new Words[500000];
		}
		
		public void resize(){
			Words[] temp=new Words[oray.length*2];
			for(int i=0;i<count;i++){
				temp[i]=oray[i];
			}
			oray=temp;
		}
		
		// increase the amount of word
		public boolean increase(String key, int num) {
			Words want = new Words(key, list.getValue(key) - num);
			int top = count;
			int bottom = 0;
			int mid = (top + bottom) / 2;
			while (top >= bottom && (!oray[mid].key.equals(key))) {
				if (oray[mid].compareTo(want) > 0)
					bottom = mid + 1;
				else
					top = mid - 1;
				mid = (bottom + top) / 2;
			}
			oray[mid].add(num);
			int current = mid;
			while (current > 0 && oray[current].compareTo(oray[current - 1]) > 0) {
				Words temp = new Words(oray[current].key, oray[current].value);
				oray[current] = oray[current - 1];
				oray[current - 1] = temp;
				current--;
			}
			return true;
		}

		// add new word
		public boolean add(Words word) {
			oray[count] = word;
			int current = count;
			while (current > 0 && oray[current].compareTo(oray[current - 1]) > 0) {
				Words temp = new Words(oray[current].key, oray[current].value);
				oray[current] = oray[current - 1];
				oray[current - 1] = temp;
				current--;
			}
			count++;
			if(oray.length-count==oray.length-10)
				resize();
			return true;
		}
		
		public String get(int n) {
			return oray[n].key;
		}
	}

	// ---------------------------------------------------------------------------Node
	// class
	public class Node {
		Node next;
		Words data;

		Node(Words data) {
			this.data = data;
		}

	}
	// private void sort() {
	// Collections.sort(list, comparator());
	// }

	// Comparator<String> comparator() {
	// return new Comparator<String>() {
	//
	// @Override
	// public int compare(String a, String b) {
	// if (a.substring(a.indexOf(" ") + 1).equals(b.substring(b.indexOf(" ") +
	// 1)))
	// return a.substring(0, a.indexOf(" ")).compareTo(b.substring(0,
	// b.indexOf(" ")));
	// return a.substring(a.indexOf(" ") + 1).compareTo(b.substring(b.indexOf("
	// ") + 1));
	// }
	// };
	// }
}
