import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
//work good:constructors,methods that add and remove members,
// 			methods that make two people pals, and that end the friendship between two pals
//			getMembers and getPals 
//			toString
//			commonPals, getPalsOfPals
//			degreesOfSeparation, membersWithDegree, areConnected

//work bad:nothing
/**
 * CSE 274 
 * @author wangb12
 *
 */
public class PalBook {

	private int size;
	private String[] list;
	private int mememberNum = 0;
	private HashMap<String, ArrayList<String>> pals = new HashMap<String, ArrayList<String>>();

	
	public PalBook() {
		this.size = 500;
		this.list = new String[size];
	}

	public PalBook(int size) {
		this.size = size;
		this.list = new String[size];
	}
	/**
	 * Adds a member to the social network. Returns true if, 
	 * at the end of the method, that member is in the network,
	 *  and false otherwise. Note that if the member already exists in the network,
	 *   the member will not be added a second time, and the method will return 
	 *   true to indicate that the member is in the network. 
	 *    The only reason this method should fail is if the social network 
	 *    has reached its maximum size.
	 * @param name
	 * @return
	 */
	public boolean addMember(String name) {
		if (mememberNum < size) {
				for(int i=0;i<mememberNum;i++){
					if(list[i].equals(name))
						return true;
			}
			list[mememberNum] = name;
			ArrayList<String> temp=new ArrayList<String>();
			pals.put(name, temp);
			mememberNum++;
			return true;
		}
		return false;
	}
	
	/**
	 * Returns true if two members are connected through a chain of pals, and
	 * false otherwise.
	 * 
	 * @param name1
	 *            the id of one member
	 * @param name2
	 *            the id of another member(not necessarily different)
	 * @return true if the members are connected, and false otherwise
	 */
	public boolean areConnected(String name1, String name2) {
		return degreesOfSeparation(name1, name2)>0;
	}

	
	/**
	 * Returns the degrees of separation between two (not necessarily distinct)
	 * members. Returns -1 if there is no way to get to one member from another
	 * through a chain of pals. For example, if John's only pal is Jane, and
	 * Norm's only pal is Jane, then: The degrees of separation between John and
	 * Jane is 1. The degrees of separation between John and Norm is 2. The
	 * degrees of separation between John and John is 0. The degrees of
	 * separation between John and Tony is -1. Note that degrees of separation
	 * is commutative. So, if degreesOfSeparation("A", "B") is 5, then
	 * degreesOfSeparation("B", "A") must also be 5.
	 * 
	 * @param name1
	 *            the id of one member
	 * @param name2
	 *            the id of another member(not necessarily different)
	 * @return The number of degrees of separation between id1 and id2, or -1 if
	 *         the two are not connected by a chain of pals.
	 */
	public int degreesOfSeparation(String name1, String name2) {
		if(name1.equals(name2))
			return 0;
		ArrayList<String> first=new ArrayList<String>();
		ArrayList<String> second=new ArrayList<String>();
		int degree=1;
		for(String pal:pals.get(name1)){
			if(pal.equals(name2))
				return degree;
			second.add(name1);
			first.add(pal);
		}
		while(first.size()>0){
			degree++;
			ArrayList<String> temp=new ArrayList<String>();
			for(int i=0;i<first.size();i++){
			for(String pal:getPals(first.get(i))){
				if(!(temp.contains(pal)||first.contains(pal)||second.contains(pal))){
					temp.add(pal);
				}
			}
			}
			for(String pal:temp){
				if(pal.equals(name2))
					return degree;
			}
			second.addAll(first);
			first=temp;
		}
		
		return -1;
	}

	
	/**
	 * Returns the pals of a particular member, sorted by id.
	 * 
	 * @param name
	 *            the id of a member
	 * @return an array of the pals of the specified member, sorted by id.
	 *         Returns an array of size zero if the specified member does not
	 *         exist, or if the member exists but has no pals.
	 */
	public String[] getPals(String name) {
		ArrayList<String> temp=pals.get(name);
		Collections.sort(temp);
		return  temp.toArray(new String[temp.size()]);
	}

	
	/**
	 * Returns an array of all members who are "pals of pals" of a specified
	 * member, but are not pals with that member. In other words, returns an
	 * array of all members who are exactly two degrees of separation from a
	 * specified member. Returns an empty array (length 0) if no members exist
	 * who are two degrees of separation from the given member. The array will
	 * be sorted by the members' ids.
	 * 
	 * @param name
	 *            the id of a member
	 * @return an array of members with two degrees of separation from the
	 *         specified member, sorted by id.
	 */
	public String[] getPalsOfPals(String name) {
		ArrayList<String> someonesPalsPal=new ArrayList<String>();
		for(int i=0;i<mememberNum;i++){
			if(degreesOfSeparation(list[i], name)==2)
				someonesPalsPal.add(list[i]);
		}
		Collections.sort(someonesPalsPal);
		return someonesPalsPal.toArray(new String[someonesPalsPal.size()]);
	}

	
	/**
	 * Returns an array of all members with a given number of degrees of
	 * separation from a specified member. Returns an empty array (length 0) if
	 * no members exist with that degree of separation from the given member.
	 * The array will be sorted by the members' ids.
	 * 
	 * @param name
	 *            the id of the specified member
	 * @param degrees
	 *            - the degrees desired (see degreesOfSeparationb for an
	 *            understanding of the various values that can be returned)
	 * @return an array of members with the given degree of separation sorted by
	 *         id.
	 */
	public String[] membersWithDegree(String name, int degrees) {
		ArrayList<String> memembers=new ArrayList<String>();
		for(int i=0;i<mememberNum;i++){
			if(degreesOfSeparation(name, list[i])==degrees)
				memembers.add(list[i]);
		}
		
		Collections.sort(memembers);
		return memembers.toArray(new String[memembers.size()]);
	}

	/**
	 * Given two members, returns an array of members who are pals with both of
	 * the two members. The array will be sorted by the members' names.
	 * 
	 * @param name1
	 *            the id of one member
	 * @param name2
	 *            the id of another member(not necessarily different)
	 * @return an array of members who are pals to both of the two specified
	 *         members sorted by id.
	 */
	public String[] commonPals(String name1, String name2) {
		ArrayList<String> commonPals=new ArrayList<String>();
		ArrayList<String> name1Pals=this.pals.get(name1);
		ArrayList<String> name2Pals=this.pals.get(name2);
		for(String palsOf1:name1Pals){
			for(String palsOf2:name2Pals){
				if(palsOf2.equals(palsOf1))
					commonPals.add(palsOf2);
			}
		}
		return commonPals.toArray(new String[commonPals.size()]);

	}

	/**
	 * Gets a count of the number of members.
	 * 
	 * @return the number of members in the social network
	 */
	public int getMemberCount() {
		return mememberNum;
	}

	/**
	 * Returns a (possibly empty) array of all members in the network, sorted by
	 * id.
	 * 
	 * @return an array of all members in the network, sorted by id. Returns an
	 *         array of length 0 if there are no members in the network.
	 */
	public String[] getMembers() {
		String[] memembers=new String[mememberNum];
		for(int i=0;i<mememberNum;i++){
			memembers[i]=list[i];
		}
		Arrays.sort(memembers);
		return memembers;
	}

	
	/**
	 * Returns a newline-separated list of members, including a comma-separated
	 * list of each member's pals. Note that the list of members should be
	 * sorted by id, and each member's list of pals should be sorted by id.
	 * 
	 * @return A newline-separated list of members, with each member's pals.
	 *         Members and pal lists sorted by id.
	 */
	public String toString() {
		ArrayList<String> temp=new ArrayList<String>();
		for(int a=0;a<mememberNum;a++){
			temp.add(list[a]);
		}
		Collections.sort(temp);
		String out="";
		for(int i=0;i<mememberNum;i++){
			out=out +temp.get(i)+Arrays.toString(getPals(temp.get(i)))+"\n";
		}
		return out;
	}

	
	/**
	 * Returns true if there exists a member in the network with the given id,
	 * and false otherwise.
	 * 
	 * @param name
	 *            the id of one member
	 * @return true if the member exists in the network, and false otherwise
	 */
	public boolean containsMember(String name) {
		for(int i=0;i<mememberNum;i++){
			if(list[i].equals(name)){
				return true;
			}
		}
		return false;
	}

	
	/**
	 * Returns true if the two specified people exist in the network, and are
	 * pals, and returns false otherwise.
	 * 
	 * @param name1
	 *            the id of one member
	 * @param name2
	 *            the id of another member
	 * @return true if the two specified people exist in the network, and are
	 *         pals, and returns false otherwise.
	 */
	public boolean arePals(String name1, String name2) {
		if(!containsMember(name1)||(!containsMember(name2)))
			return false;
		for (String pal:pals.get(name1)) {
			if(pal.equals(name2))
			return true;
		}
		return false;
	}

	
	/**
	 * If id1 and id2 are members in the network, then makes them pals. Returns
	 * true if, at the end of the method, id1 and id2 are pals (which could mean
	 * they were pals before this method was invoked), and returns false
	 * otherwise (which really would only happen if id1 or id2 were not a member
	 * in the network.
	 * 
	 * @param name1
	 *            the id of one member
	 * @param name2
	 *            the id of another member
	 * @return
	 */
	public boolean makePals(String name1, String name2) {
		if(!containsMember(name1)||(!containsMember(name2)))
			return false;
		if(arePals(name1, name2))
			return true;
		pals.get(name1).add(name2);
		pals.get(name2).add(name1);
		return true;
	}

	
	/**
	 * Removes id1 from id2's pal list, and removes id2 from id1's pal list.
	 * Does nothing id1 or id2 are not members, or if they are members but are
	 * not pals.
	 * 
	 * @param name1
	 *            the id of one member
	 * @param name2
	 *            the id of another member
	 */
	public void endPals(String name1, String name2) {
		if(arePals(name1, name2)){
			pals.get(name1).remove(name2);
			pals.get(name2).remove(name1);
			
		}
	}


	/**
	 * Removes member from the social network. If member is pals with anyone,
	 * all traces of that member will be gone. That is, member will be removed
	 * from everyone's pal list. Does nothing if member is not in the social
	 * network.
	 * 
	 * @param name
	 *            the id of the member to be removed
	 */
	public void removeMember(String name) {
		for(int i=0;i<mememberNum;i++){
			endPals(list[i],name);
		}
		for(int i=0;i<mememberNum;i++){
			if(list[i].equals(name)){
				list[i]=list[mememberNum-1];
				list[size-1]=null;
				mememberNum--;
			}
		}
	}
}
