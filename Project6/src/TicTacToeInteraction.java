import java.util.Scanner;

/**
 * @author Bo Wang wangb12
 *
 */
public class TicTacToeInteraction {

	public static void main(String[] args) {
		TicTacToe t = new TicTacToe();
		String[] board = new String[9];
		int humanMove;
		for (int i = 0; i < 9; i++) {
			board[i] = "-";
		}
		Scanner in = new Scanner(System.in);
		//there is only one way to win computer in my program
		//Can you find the only way?
		//hint: start from corner
		System.out.println("How dare you challenge me! Human!! You would never win!");
		while (!isFull(board) && !xWin(board) && !oWin(board)) {
			System.out.println(toString(board));
			System.out.println("Enter your move, human.(from 0 to 8)");
			humanMove = in.nextInt();
			while (!isValid(humanMove, board)) {
				System.out.println("Human, Your number is not valid."
						+ "But I am a tolerant computer. You still have changces");
				humanMove = in.nextInt();
			}
			board[humanMove] = "x";
			if (isFull(board))
				break;
			System.out.println("Computer move is: " + t.get(board));
			board[t.get(board)] = "o";
		}
		in.close();
		if (xWin(board))
			System.out.println("That is impossiable!!");
		else if (oWin(board))
			System.out.println("Told you. GG :D");
		else
			System.out.println("Is this the best you can do? =.=");
	}

	public static String toString(String[] board) {
		return board[0] + board[1] + board[2] + "\n" + board[3] + board[4] + board[5] + "\n" + board[6] + board[7]
				+ board[8];
	}

	public static boolean xWin(String[] board) {
		if (board[0].equals(board[1]) && board[1].equals(board[2]) && board[0].equals("x")
				|| board[0].equals(board[3]) && board[3].equals(board[6]) && board[0].equals("x")
				|| board[0].equals(board[4]) && board[4].equals(board[8]) && board[0].equals("x")
				|| board[4].equals(board[1]) && board[1].equals(board[7]) && board[1].equals("x")
				|| board[2].equals(board[5]) && board[5].equals(board[8]) && board[2].equals("x")
				|| board[2].equals(board[4]) && board[4].equals(board[6]) && board[2].equals("x")
				|| board[3].equals(board[4]) && board[4].equals(board[5]) && board[3].equals("x")
				|| board[6].equals(board[7]) && board[7].equals(board[8]) && board[8].equals("x"))
			return true;
		return false;

	}

	public static boolean oWin(String[] board) {
		if (board[0].equals(board[1]) && board[1].equals(board[2]) && board[0].equals("o")
				|| board[0].equals(board[3]) && board[3].equals(board[6]) && board[0].equals("o")
				|| board[0].equals(board[4]) && board[4].equals(board[8]) && board[0].equals("o")
				|| board[4].equals(board[1]) && board[1].equals(board[7]) && board[1].equals("o")
				|| board[2].equals(board[5]) && board[5].equals(board[8]) && board[2].equals("o")
				|| board[2].equals(board[4]) && board[4].equals(board[6]) && board[2].equals("o")
				|| board[3].equals(board[4]) && board[4].equals(board[5]) && board[3].equals("o")
				|| board[6].equals(board[7]) && board[7].equals(board[8]) && board[8].equals("o"))
			return true;
		return false;
	}

	public static boolean isFull(String[] board) {
		int count = 0;
		for (String s : board) {
			if (s.equals("-"))
				count++;
		}
		return count == 0;
	}

	public static boolean isValid(int position, String[] board) {
		if (position > 8 || position < 0)
			return false;
		if (!board[position].equals("-"))
			return false;
		return true;
	}
}
