import java.util.Arrays;

/**
 * @author Bo Wang wangb12
 *
 */
public class TicTacToe {
	private String[] board = new String[9];
	private HashedDictionary<String, Integer> move = new HashedDictionary<>();
	private String x = "x";
	private String o = "o";

	public TicTacToe() {
		createBoard();
		addBoard(board);
	}

	/**
	 * 
	 * @param board
	 * @return
	 */
	public int getBestMove(String board) {
		if (board == null)
			return -1;
		board = board.toLowerCase();
		String[] newBoard = new String[9];
		for (int i = 0; i < 9; i++) {
			newBoard[i] = board.substring(i, i + 1);
		}
		if (isFull(newBoard))
			return -1;
		if (getX(newBoard) < getO(newBoard))
			return -1;
		return move.getValue(Arrays.toString(newBoard));
	}

	/**
	 * get the best move for given board
	 * 
	 * @param board
	 * @return
	 */
	public int get(String[] board) {
		return move.getValue(Arrays.toString(board));
	}

	/**
	 * find empty place in the board
	 * 
	 * @param board
	 * @return the position of the board, -1 if is full
	 */
	public int findEmpty(String[] board) {
		for (int i = 0; i < 9; i++) {
			if (board[i].equals("-"))
				return i;
		}
		return -1;
	}

	/**
	 * create an empty board
	 */
	private void createBoard() {
		for (int i = 0; i < 9; i++) {
			this.board[i] = "-";
		}
	}

	/**
	 * add all possible board into dictionary
	 * 
	 * @param board
	 * @return
	 */
	private void addBoard(String[] board) {
		for (int first = 0; first < 9; first++) {
			if (board[first].equals("-")) {// x 1
				board[first] = x;
				move.add(Arrays.toString(board), makeBestMove(board));
				for (int firstO = 0; firstO < 9; firstO++) {// o 1
					if (board[firstO].equals("-")) {
						board[firstO] = o;
						move.add(Arrays.toString(board), makeBestMove(board));
						for (int second = 0; second < 9; second++) {// x 2
							if (board[second].equals("-")) {
								board[second] = x;
								move.add(Arrays.toString(board), makeBestMove(board));
								for (int secondO = 0; secondO < 9; secondO++) {// o
																				// 2
									if (board[secondO].equals("-")) {
										board[secondO] = o;
										move.add(Arrays.toString(board), makeBestMove(board));
										for (int third = 0; third < 9; third++) {// x
																					// 3
											if (board[third].equals("-")) {
												board[third] = x;
												move.add(Arrays.toString(board), makeBestMove(board));
												for (int thirdO = 0; thirdO < 9; thirdO++) {// o
																							// 3
													if (board[thirdO].equals("-")) {
														board[thirdO] = o;
														move.add(Arrays.toString(board), makeBestMove(board));
														for (int fourth = 0; fourth < 9; fourth++) {// x
																									// 4
															if (board[fourth].equals("-")) {
																board[fourth] = x;
																move.add(Arrays.toString(board), makeBestMove(board));
																for (int fourthO = 0; fourthO < 9; fourthO++) {// o
																												// 4
																	if (board[fourthO].equals("-")) {
																		board[fourthO] = o;
																		move.add(Arrays.toString(board),
																				makeBestMove(board));

																		board[fourthO] = "-";
																	}
																}
																board[fourth] = "-";
															}
														}
														board[thirdO] = "-";
													}
												}
												board[third] = "-";
											}
										}
										board[secondO] = "-";
									}
								}
								board[second] = "-";
							}
						}
						board[firstO] = "-";
					}
				}
				board[first] = "-";
			}
		}
	}

	/**
	 * make best move for the given board if there is no place to move, return
	 * -1
	 * 
	 * @param board
	 * @return
	 */
	private int makeBestMove(String[] board) {
		// if x gonna win, block it
		// if o gonna win, make it
		if (board[5].equals(board[8]) && (board[8].equals(x) || board[8].equals(o)) && board[2].equals("-")
				|| board[6].equals(board[4]) && (board[4].equals(x) || board[4].equals(o)) && board[2].equals("-")
				|| board[0].equals(board[1]) && (board[1].equals(x) || board[1].equals(o)) && board[2].equals("-"))
			return 2;
		else if (board[4].equals(board[7]) && (board[7].equals(x) || board[7].equals(o)) && board[1].equals("-")
				|| board[0].equals(board[2]) && (board[2].equals(x) || board[2].equals(o)) && board[1].equals("-"))
			return 1;
		else if (board[6].equals(board[3]) && (board[3].equals(x) || board[3].equals(o)) && board[0].equals("-")
				|| board[4].equals(board[8]) && (board[8].equals(x) || board[8].equals(o)) && board[0].equals("-")
				|| board[1].equals(board[2]) && (board[2].equals(x) || board[2].equals(o)) && board[0].equals("-"))
			return 0;
		else if (board[2].equals(board[8]) && (board[8].equals(x) || board[8].equals(o)) && board[5].equals("-")
				|| board[3].equals(board[4]) && (board[4].equals(x) || board[4].equals(o)) && board[5].equals("-"))
			return 5;
		else if (board[0].equals(board[8]) && (board[8].equals(x) || board[8].equals(o)) && board[4].equals("-")
				|| board[1].equals(board[7]) && (board[7].equals(x) || board[7].equals(o)) && board[4].equals("-")
				|| board[6].equals(board[2]) && (board[2].equals(x) || board[2].equals(o)) && board[4].equals("-")
				|| board[3].equals(board[5]) && (board[5].equals(x) || board[5].equals(o)) && board[4].equals("-"))
			return 4;
		else if (board[0].equals(board[6]) && (board[6].equals(x) || board[6].equals(o)) && board[3].equals("-")
				|| board[4].equals(board[5]) && (board[5].equals(x) || board[5].equals(o)) && board[3].equals("-"))
			return 3;
		else if (board[6].equals(board[7]) && (board[7].equals(x) || board[7].equals(o)) && board[8].equals("-")
				|| board[0].equals(board[4]) && (board[4].equals(x) || board[4].equals(o)) && board[8].equals("-")
				|| board[2].equals(board[5]) && (board[5].equals(x) || board[5].equals(o)) && board[8].equals("-"))
			return 8;
		else if (board[6].equals(board[8]) && (board[8].equals(x) || board[8].equals(o)) && board[7].equals("-")
				|| board[1].equals(board[4]) && (board[4].equals(x) || board[4].equals(o)) && board[7].equals("-"))
			return 7;
		else if (board[2].equals(board[4]) && ((board[4].equals(x) || board[4].equals(o)) || board[4].equals(o))
				&& board[6].equals("-")
				|| board[7].equals(board[8]) && (board[8].equals(x) || board[8].equals(o)) && board[6].equals("-")
				|| board[0].equals(board[3]) && (board[3].equals(x) || board[3].equals(o)) && board[6].equals("-"))
			return 6;
		// for no one win, first consider the center, then corner, last other
		// place
		else if (board[4].equals("-"))
			return 4;
		else if (board[0].equals("-"))
			return 0;
		else if (board[2].equals("-"))
			return 2;
		else if (board[6].equals("-"))
			return 6;
		else if (board[8].equals("-"))
			return 8;
		return findEmpty(board);
	}

	/**
	 * check whether board is full or not
	 * 
	 * @param board
	 * @return return true if is full
	 */
	private boolean isFull(String[] board) {
		int count = 0;
		for (String a : board) {
			if (a.equals("-"))
				count++;
		}
		return count == 0;
	}

	/**
	 * get how many o in the board
	 * 
	 * @param board
	 * @return
	 */
	private int getO(String[] board) {
		int count = 0;
		for (String o : board) {
			if (o.equals("o"))
				count++;
		}
		return count;
	}

	/**
	 * get how many x in the board
	 * 
	 * @param board
	 * @return
	 */
	private int getX(String[] board) {
		int count = 0;
		for (String x : board) {
			if (x.equals("x"))
				count++;
		}
		return count;
	}
}
